import styled from 'styled-components';
import { Helmet } from 'react-helmet-async';
import { Card } from '../../components/Card';
import { Stack } from '../../components/Stack';
import { PageWrapper } from '../../components/PageWrapper';
import { Input } from '../../components/Input';
import { Checkbox } from '../../components/Checkbox';
import { InputGroup } from '../../components/InputGroup';
import { Form } from '../../components/Form';
import { Button } from '../../components/Button';
import { AuthenticationScreen } from '../../components/AuthenticationScreen';
import Brand from '../../assets/images/brand.svg';
import Back from '../../assets/images/back.svg';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Lead Score</title>
        <meta name="description" content="Lead Score" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
        />
      </Helmet>
      <PageWrapper className="page-wrapper">
        <AuthenticationScreen className="authentication-screen login">
          <LoginScreen className="login-screen">
            <figure className="brand">
              <img src={Brand} alt="Brand" />
            </figure>
            <h2>Login</h2>
            <Form className="form">
              <InputGroup />
              <InputGroup />
              <Stack className="stack between">
                <Checkbox />
                <span className="forgot-password">Forgot Password?</span>
              </Stack>
              <Button>Login</Button>
            </Form>
          </LoginScreen>
        </AuthenticationScreen>
        <AuthenticationScreen className="authentication-screen forgot-password">
          <ForgotPassword className="login-screen">
            <figure className="back">
              <img src={Back} alt="Back" />
            </figure>
            <h1>Forgot Password</h1>
            <p className="lead">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever
            </p>
            <Form className="form">
              <Input />
              <Button>Reset </Button>
            </Form>
          </ForgotPassword>
        </AuthenticationScreen>
        <Input />
        <Button>Change password</Button>
        <Card className="card">
          <Stack className="stack between">
            <h3>Ramesh Kumar</h3>
            <Stack className="stack">
              <small>Enq.on </small>
              <small>: 01/12/22</small>
            </Stack>
          </Stack>
          <Stack className="stack between">
            <h4>Toyota Etios Liva 1.2 GX </h4>
            <Stack className="stack">
              <small>Enq.ID</small>
              <small>: 101245</small>
            </Stack>
          </Stack>
          <Stack className="stack between">
            <small>Diesel | 2012</small>
            <span>Interested</span>
          </Stack>
        </Card>
      </PageWrapper>
    </>
  );
}

const LoginScreen = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 20vh;
  h2 {
    text-align: center;
    margin-top: 2.5rem;
  }
  .brand {
    max-width: 170px;
    margin-left: auto;
    margin-right: auto;
  }
  .form {
    margin-top: 1.5rem;
  }
  .forgot-password {
    font-size: 12px;
    color: #000000;
    text-decoration: none;
  }
`;

const ForgotPassword = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 2rem;
  .lead {
    margin-top: 12px;
  }
  .brand {
    max-width: 170px;
    margin-left: auto;
    margin-right: auto;
  }
  .form {
    margin-top: 1.5rem;
  }
  .forgot-password {
    font-size: 12px;
    color: #000000;
    text-decoration: none;
  }
`;
