import { createGlobalStyle } from 'styled-components';
/* istanbul ignore next */
export const GlobalStyle = createGlobalStyle`
    html,
    body {
        height: 100%;
        width: 100%;
        line-height: 1.5;
    }

    * {
        box-sizing: border-box;
        transition: all 0.15s ease-out 0s;
        font-family: 'Roboto', -apple-system, BlinkMacSystemFont;
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
        text-rendering: optimizeLegibility;
        word-break: break-word;
        padding: 0;
        margin: 0;
        font-family: 'Poppins', sans-serif;
    }

    body {
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        line-height: 1.5;
        color: #161719;
    }
    
    p,
    label {
        line-height: 1.5em;
    }

    .lead {
        color: #A4A4A4;
    }

    h1 {
        font-weight: 700;
        font-size: 20px;
        color: #000000;
    }

    h2 {
        font-weight: 600;
        font-size: 18px;
        color: #000000;
    }

    h3 {
        font-weight: 700;
        font-size: 14px;
        color: #000000;
    }

    h4 {
        font-weight: 600;
        font-size: 12px;
        color: #161719;
    }

    input,
    select,
    button {
        font-family: inherit;
        font-size: inherit;
    }
`;
