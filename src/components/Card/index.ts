import styled from 'styled-components';

export const Card = styled.div`
  background-color: #ffffff;
  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.07);
  border-radius: 10px;
  padding: 12px 16px;
  > * {
    margin-top: 5px;
  }
  small {
    font-size: 11px;
    line-height: 1.45;
    color: #455a64;
  }
  span {
    font-size: 12px;
    color: #05ad20;
  }
`;
