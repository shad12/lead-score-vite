import styled from 'styled-components';

export const AuthenticationScreen = styled.div`
  min-height: 100vh;
  &.login {
    input {
      height: 48px;
    }
  }
`;
