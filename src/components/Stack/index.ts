import styled from 'styled-components';

export const Stack = styled.div`
  display: flex;
  align-items: center;
  &.between {
    justify-content: space-between;
  }
`;
