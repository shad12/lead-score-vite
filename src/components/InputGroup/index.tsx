import styled from 'styled-components';

import Eye from '../../assets/images/eye.svg';

export function InputGroup() {
  return (
    <InputGroupStyled className="input-group icon-holder">
      <InputField id="name" type="text" placeholder="Name" />
      <figure className="icon">
        <img src={Eye} alt="eye" />
      </figure>
    </InputGroupStyled>
  );
}

const InputGroupStyled = styled.div`
  &.icon-holder {
    position: relative;
    input {
      padding-right: 54px;
    }
    .icon {
      display: flex;
    }
  }
  .icon {
    width: 22px;
    height: 15px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 16px;
    display: none;
  }
`;

const InputField = styled.input`
  border: 1px solid #c4c4c4;
  border-radius: 5px;
  width: 100%;
  height: 40px;
  color: #000000;
  font-weight: 500;
  padding-left: 16px;
  padding-right: 16px;
  &::-webkit-input-placeholder {
    color: #a4a4a4;
    font-weight: 400;
  }
  &:-moz-placeholder {
    color: #a4a4a4;
    font-weight: 400;
  }
  &:-ms-input-placeholder {
    color: #a4a4a4;
    font-weight: 400;
  }
  &:-moz-placeholder {
    color: #a4a4a4;
    font-weight: 400;
  }
`;
