import styled from 'styled-components';

export const Button = styled.button`
  background-color: #534ea1;
  border-radius: 5px;
  width: 100%;
  min-height: 48px;
  font-weight: 600;
  font-size: 16px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: #534ea1 1px solid;
  box-shadow: none;
  text-transform: capitalize;
`;
