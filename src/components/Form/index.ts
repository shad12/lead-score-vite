import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  > * {
    &:not(:first-child) {
      margin-top: 1.5rem;
    }
  }
`;
