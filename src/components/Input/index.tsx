import styled from 'styled-components';
import { InputGroup } from '../../components/InputGroup';

export function Input() {
  return (
    <InputWrapper className="input-wrapper">
      <Label htmlFor="name">
        <span>Name</span>
        <InputGroup />
      </Label>
    </InputWrapper>
  );
}

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.label`
  display: flex;
  flex-direction: column;
  span {
    font-size: 12px;
    line-height: 18px;
    color: #1e1e1e;
    margin-bottom: 2px;
  }
`;
