import styled from 'styled-components';

export const PageWrapper = styled.main`
  max-width: 680px;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  padding: 0 2rem 2rem;
`;
