import styled from 'styled-components';

export function Checkbox() {
  return (
    <CheckboxStyled htmlFor="remember" className="checkbox">
      <InputField id="remember" type="checkbox" name="remember" />
      <span>Remember Me</span>
    </CheckboxStyled>
  );
}

const CheckboxStyled = styled.label`
  display: flex;
  align-items: center;
  cursor: pointer;
  span {
    position: relative;
    padding-left: 30px;
    color: #000;
    font-size: 12px;
    &:before {
      content: '';
      width: 18px;
      height: 18px;
      border-radius: 6px;
      border: 1px solid #e4e3f4;
      position: absolute;
      left: 0;
      top: 1px;
    }
  }
  input {
    &:checked {
      & + span {
        &:before {
          background-color: #e4e3f4;
        }
      }
    }
  }
`;

const InputField = styled.input`
  display: none;
`;
